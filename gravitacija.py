print("OIS je zakon!")

c = 6.674 * 10**(-11)
r = 6.371 * 10**6
m = 5.972*10**24

def gravitacijskiPospesek(h):
    return((c*m)/((r+h*1000)**2))
    
def izpis(h):
    print(gravitacijskiPospesek(h))

tests = [0.0, 10.0, 1000.0, 100000.0]

for i in tests:
    izpis(i)